module gitlab.com/NotDzhedai/svitlo-check

// +heroku goVersion go1.16
go 1.16

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/go-redis/redis v6.15.5+incompatible
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/gocolly/colly/v2 v2.1.0
	github.com/jasonlvhit/gocron v0.0.1
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
)
