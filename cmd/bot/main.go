package main

import (
	"log"
	"path/filepath"

	"github.com/BurntSushi/toml"
	"github.com/go-redis/redis"
	"gitlab.com/NotDzhedai/svitlo-check/internal/botctl"
	"gitlab.com/NotDzhedai/svitlo-check/internal/config"
)

const (
	PATH_TO_CONFIG = "config.toml"
)

func main() {
	// Config
	cfg, err := connectConfig()
	if err != nil {
		log.Fatal("Error with config: ", err)
	}

	// Database
	redisDB, err := newRedisDb(cfg)
	if err != nil {
		log.Fatal(err)
	}
	defer redisDB.Close()
	log.Println("Redis connected")

	// Botctl
	tgControl, err := botctl.NewTelegramControl(cfg, redisDB)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Telegram bot control created")

	// Run bot
	log.Println("*** Starting bot ***")
	if botErr := tgControl.RunBot(); err != nil {
		log.Fatal(botErr)
	}
}

func connectConfig() (*config.Config, error) {
	conf := config.NewConfig()
	tomlFile, err := filepath.Abs(PATH_TO_CONFIG)
	if err != nil {
		return nil, err
	}

	_, err = toml.DecodeFile(tomlFile, conf)
	if err != nil {
		return nil, err
	}

	return conf, nil
}

func newRedisDb(cfg *config.Config) (*redis.Client, error) {
	client := redis.NewClient(&redis.Options{
		Addr:     cfg.RedisConnectionAddress,
		Password: "",
		DB:       cfg.RedisLayout,
	})

	if err := client.Ping().Err(); err != nil {
		return nil, err
	}

	return client, nil
}
