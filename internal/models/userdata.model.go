package models

type UserData struct {
	ChatId   int64  `json:"chat_id"`
	City     string `json:"district"`
	District string `json:"settlement"`
	Street   string `json:"street"`
}
