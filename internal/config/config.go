package config

type Config struct {
	TelegramToken          string `toml:"TELEGRAM_TOKEN"`
	TelegramDebug          bool   `toml:"TELEGRAM_DEBUG"`
	RedisConnectionAddress string `toml:"REDIS_CONNECTION_ADDRESS"`
	RedisLayout            int    `toml:"REDIS_LAYOUT"`
	AppMode                string `toml:"APP_MODE"`
	TimeToSendData         string `toml:"TIME_TO_SEND_DATA"`
}

func NewConfig() *Config {
	return &Config{}
}
