package scraper

import (
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/gocolly/colly/v2"
	"github.com/patrickmn/go-cache"
	"gitlab.com/NotDzhedai/svitlo-check/internal/config"
	"gitlab.com/NotDzhedai/svitlo-check/internal/models"
	"gitlab.com/NotDzhedai/svitlo-check/internal/storage"
)

const (
	DNIPRO              = "DNIPRO"
	SINVE               = "SINVE"
	URL_TO_CHECK_SINVE  = "https://www.dtek-dnem.com.ua/ua/outages?query=&page=1&rem=%D0%A1%D0%B8%D0%BD%D0%B5%D0%BB%D1%8C%D0%BD%D0%B8%D0%BA%D1%96%D0%B2%D1%81%D1%8C%D0%BA%D0%B0+%D0%B4%D1%96%D0%BB%D1%8C%D0%BD%D0%B8%D1%86%D1%8F&status=-1&shutdown-date=-1&inclusion-date=-1&placement-date=-1"
	URL_TO_CHECK_DNIPRO = "https://www.dtek-dnem.com.ua/ua/outages?query=&page=1&rem=%D0%94%D0%BD%D1%96%D0%BF%D1%80%D0%BE%D0%B2%D1%81%D1%8C%D0%BA%D0%B8%D0%B9+%D0%A0%D0%95%D0%9C&status=-1&shutdown-date=-1&inclusion-date=-1&placement-date=-1&page-limit=50"
)

type ScheduleElement struct {
	Settlement   string
	Street       string
	Type         string
	ShutdownTime string
	RecoveryTime string
}

type scraper struct {
	cfg       *config.Config
	collector *colly.Collector
}

func NewScraper(cfg *config.Config) *scraper {
	c := colly.NewCollector()

	c.AllowURLRevisit = true
	c.SetClient(&http.Client{
		Timeout: 0,
	})

	return &scraper{
		collector: c,
		cfg:       cfg,
	}
}

func (s *scraper) GetActualData(userValue *models.UserData) (string, error) {
	// Track time to make req
	start := time.Now()
	// cache
	store := storage.GetStorage()
	// data aff all els to store in cache
	var data []ScheduleElement
	// data for user
	var dataToReturn []ScheduleElement

	s.collector.OnHTML("a[href]", func(e *colly.HTMLElement) {
		if e.Attr("class") == "next" {
			link := e.Attr("href")
			log.Println(link)
			e.Request.Visit("https://www.dtek-dnem.com.ua" + link)
		}
	})

	s.collector.OnHTML("table.table.table-shutdowns tbody", func(e *colly.HTMLElement) {
		e.ForEach("tr", func(i int, h *colly.HTMLElement) {
			if i != 0 {
				item := ScheduleElement{
					ShutdownTime: h.ChildText("td:nth-child(1)"),
					RecoveryTime: h.ChildText("td:nth-child(2)"),
					Settlement:   strings.ReplaceAll(h.ChildText("td:nth-child(3)"), " ", ""),
					Street:       strings.ReplaceAll(h.ChildText("td:nth-child(5)"), " ", ""),
					Type:         h.ChildText("td:nth-child(7)"),
				}
				data = append(data, item)
			}
		})
	})

	url := getUrl(userValue.City)
	log.Println(fmt.Sprintf("VISITING (%s) %s", userValue.City, url))

	dataFromCache, found := store.Get(userValue.City)
	if !found {
		s.collector.Visit(url)
		store.Add(userValue.City, data, cache.NoExpiration)
	} else {
		data = dataFromCache.([]ScheduleElement)
	}

	for _, i := range data {
		if contains(i.Settlement, i.Street, userValue) {
			dataToReturn = append(dataToReturn, i)
		}
	}

	end := time.Since(start)
	log.Printf("time fo handle req: %s", end)

	return dataToString(dataToReturn), nil
}
