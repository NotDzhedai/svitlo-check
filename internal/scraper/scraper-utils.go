package scraper

import (
	"strings"

	"gitlab.com/NotDzhedai/svitlo-check/internal/models"
)

func contains(district, street string, u *models.UserData) bool {
	return strings.Contains(strings.ToLower(district), u.District) && strings.Contains(strings.ToLower(street), u.Street)
}

func getUrl(city string) string {
	switch city {
	case DNIPRO:
		return URL_TO_CHECK_DNIPRO
	case SINVE:
		return URL_TO_CHECK_SINVE
	default:
		return "дана дільниця ще не реалізована, або не існує..."
	}
}

func dataToString(data []ScheduleElement) string {
	builder := strings.Builder{}

	if len(data) == 0 {
		return "Відключень для вашої адреси не знайдено"
	}

	builder.WriteString("Відключення: \n\n")
	for _, d := range data {
		builder.WriteString("Поселення: " + d.Settlement + "\n")
		builder.WriteString("Вулиці: " + d.Street + "\n")
		builder.WriteString("Тип: " + d.Type + "\n")
		builder.WriteString("Час відключення: " + d.ShutdownTime + "\n")
		builder.WriteString("Час відновлення: " + d.RecoveryTime + "\n")
		builder.WriteString("\n")
	}

	return builder.String()
}
