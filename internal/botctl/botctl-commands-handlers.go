package botctl

import (
	"encoding/json"
	"fmt"
	"log"
	"strings"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/NotDzhedai/svitlo-check/internal/models"
	"gitlab.com/NotDzhedai/svitlo-check/internal/scraper"
	"gitlab.com/NotDzhedai/svitlo-check/internal/storage"
)

const (
	commandManual    = "manual"
	commandCheck     = "check"
	commandSubscribe = "subscribe"
	commandMessage   = "message"
)

func (b *Botctl) handleCommands(message *tgbotapi.Message) error {
	switch message.Command() {
	case commandCheck:
		return b.handleCheckCommand(message)
	case commandSubscribe:
		return b.handleSubscribeCommand(message)
	case commandManual:
		return b.handleManualCommand(message)
	case commandMessage:
		return b.handleMessageCommand(message)
	default:
		return b.handleUnknown(message)
	}
}

func (b *Botctl) handleMessageCommand(message *tgbotapi.Message) error {
	if message.From.UserName != "NotDzhedai" {
		return errForbidden
	}

	messageText := message.CommandArguments()
	b.sendMessageToUsers(messageText)

	return nil
}

func (b *Botctl) handleManualCommand(message *tgbotapi.Message) error {
	sb := strings.Builder{}
	sb.WriteString("\n* Використовуй команду /subscribe для підписки на отримання щоденних повідомлень\n")
	sb.WriteString("\n* Використовуй команду /check для отримання інформації про відключення \n")
	sb.WriteString("\n Інформація оновлюється з інтервалом в один день. Актуальна інформація находить в районі 20:30 кожного дня.")

	msg := tgbotapi.NewMessage(message.Chat.ID, sb.String())
	b.bot.Send(msg)

	return nil
}

func (b *Botctl) handleSubscribeCommand(message *tgbotapi.Message) error {
	var numericKeyboard = tgbotapi.NewInlineKeyboardMarkup(
		tgbotapi.NewInlineKeyboardRow(
			tgbotapi.NewInlineKeyboardButtonData("Дніпро", "DNIPRO"),
			tgbotapi.NewInlineKeyboardButtonData("Cинельникове", "SINVE"),
		),
	)

	msg := tgbotapi.NewMessage(
		message.Chat.ID,
		"Оберіть ваше місто",
	)
	msg.ReplyMarkup = numericKeyboard
	b.bot.Send(msg)

	return nil
}

func (b *Botctl) handleCheckCommand(message *tgbotapi.Message) error {
	val, err := b.redis.Get(message.From.UserName).Result()
	if err != nil {
		log.Println("ERROR [handleCheckCommand] get user from redis")
		return errNotFound
	}

	var userVal *models.UserData
	err = json.Unmarshal([]byte(val), &userVal)
	if err != nil {
		log.Println("ERROR [handleCheckCommand] unmarshal value from redis")
		return err
	}

	checkingMsg := tgbotapi.NewMessage(
		message.Chat.ID,
		fmt.Sprintf("Йде пошук за вашою адресою: \n%s - %s - %s \n(може зайняти декілька хвилин)", userVal.City, userVal.District, userVal.Street),
	)
	b.bot.Send(checkingMsg)

	go func(message *tgbotapi.Message) {
		scraper := scraper.NewScraper(b.cfg)
		data, err := scraper.GetActualData(userVal)
		if err != nil {
			log.Println("ERROR [handleCheckCommand] get data from scraper: ", err)
			msg := tgbotapi.NewMessage(message.Chat.ID, "Проблеми зі збором даних. Спробуйте пізніше")
			b.bot.Send(msg)
			return
		}

		msg := tgbotapi.NewMessage(message.Chat.ID, data)
		b.bot.Send(msg)
	}(message)

	return nil
}

func (b *Botctl) handleMessage(message *tgbotapi.Message) error {
	msg := tgbotapi.NewMessage(message.Chat.ID, "Я ще не вмію відповідати на повідомлення")
	b.bot.Send(msg)

	return nil
}

func (b *Botctl) sendInfoToUsers() {
	log.Println("Starting sendInfoToUsers")
	store := storage.GetStorage()
	store.Flush()
	info, err := b.redis.Keys("*").Result()
	if err != nil {
		log.Println("sendInfoToUsers: ", err)
	}
	if len(info) == 0 {
		return
	}
	for _, i := range info {
		i := i
		go func(i string) {
			val := b.redis.Get(i).Val()
			var user *models.UserData

			_ = json.Unmarshal([]byte(val), &user)

			scraper := scraper.NewScraper(b.cfg)
			data, err := scraper.GetActualData(user)
			if err != nil {
				log.Println("sendInfoToUsers: ", err)
			}

			msg := tgbotapi.NewMessage(user.ChatId, data)
			b.bot.Send(msg)
		}(i)
	}
}

func (tc *Botctl) sendMessageToUsers(message string) {
	log.Println("Starting sendMessageToUsers")
	info, err := tc.redis.Keys("*").Result()
	if err != nil {
		log.Println("sendMessageToUsers: ", err)
	}
	if len(info) == 0 {
		return
	}
	for _, i := range info {
		i := i
		go func(i string) {
			val := tc.redis.Get(i).Val()
			var user *models.UserData

			_ = json.Unmarshal([]byte(val), &user)

			msg := tgbotapi.NewMessage(user.ChatId, message)
			tc.bot.Send(msg)
		}(i)
	}
}
