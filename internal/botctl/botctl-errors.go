package botctl

import (
	"errors"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

var (
	// Errors
	errRedisSet        = errors.New("error to set data to redis")
	errRedisGet        = errors.New("error to get data to redis")
	errScraperResponse = errors.New("error to get data from scraper ")
	errNotFound        = errors.New("record not found")
	errForbidden       = errors.New("requires the role of admin")
	errMarshal         = errors.New("error with json marshal")
)

func (b *Botctl) handleErrors(chatId int64, err error) {
	msg := tgbotapi.NewMessage(chatId, "Сталася невідома помилка")

	switch err {
	case errRedisSet:
		msg.Text = "Сталася помилка при додаванні даних в базу даних"
		b.bot.Send(msg)
	case errRedisGet:
		msg.Text = "Сталася помилка при отриманні даних із бази даних"
		b.bot.Send(msg)
	case errScraperResponse:
		msg.Text = "Проблеми зі збором даних. Спробуйте пізніше"
		b.bot.Send(msg)
	case errNotFound:
		msg.Text = "Не можу знайти вашу підписку в базі. Дивись інструкцію на /manual"
		b.bot.Send(msg)
	case errForbidden:
		msg.Text = "Для цієї дії у вас немає потрібних дозволів"
		b.bot.Send(msg)
	case errMarshal:
		msg.Text = "Проблема з JSON [SERVER ERROR]"
		b.bot.Send(msg)
	default:
		b.bot.Send(msg)
	}
}
