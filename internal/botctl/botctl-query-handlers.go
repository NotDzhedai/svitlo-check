package botctl

import (
	"encoding/json"
	"fmt"
	"log"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/NotDzhedai/svitlo-check/internal/models"
)

func (b *Botctl) handleCallbackQuery(query *tgbotapi.CallbackQuery) error {
	switch query.Data {
	case "DNIPRO", "SINVE":
		return b.registerCity(query)
	case "центральний", "новокодацький", "веселе", "синельникове":
		return b.registerDistrict(query)
	case "хенсонська", "метробудівська", "гагаріна", "ягідна":
		return b.registerStreet(query)
	default:
		return b.handleUnknown(query.Message)
	}
}

func (b *Botctl) registerCity(query *tgbotapi.CallbackQuery) error {
	username := query.From.UserName

	userValue := models.UserData{
		ChatId:   query.Message.Chat.ID,
		City:     query.Data,
		District: "",
		Street:   "",
	}

	userValueStr, err := json.Marshal(&userValue)
	if err != nil {
		log.Println("ERROR [registerCity] marshal user value: ", err)
		return errMarshal
	}

	err = b.redis.Set(username, userValueStr, 0).Err()
	if err != nil {
		log.Println("ERROR [registerCity] saving record to redis: ", err)
		return errRedisSet
	}

	var keyboard tgbotapi.InlineKeyboardMarkup

	if query.Data == "DNIPRO" {
		keyboard = tgbotapi.NewInlineKeyboardMarkup(
			tgbotapi.NewInlineKeyboardRow(
				tgbotapi.NewInlineKeyboardButtonData("Центральний", "центральний"),
				tgbotapi.NewInlineKeyboardButtonData("Новокодацький", "новокодацький"),
			),
		)
	} else if query.Data == "SINVE" {
		keyboard = tgbotapi.NewInlineKeyboardMarkup(
			tgbotapi.NewInlineKeyboardRow(
				tgbotapi.NewInlineKeyboardButtonData("Веселе", "веселе"),
				tgbotapi.NewInlineKeyboardButtonData("Cинельникове", "синельникове"),
			),
		)
	}

	b.bot.AnswerCallbackQuery(tgbotapi.NewCallback(query.ID, "Збережено!"))

	msg := tgbotapi.NewMessage(query.Message.Chat.ID, "Обери свій район")
	msg.ReplyMarkup = keyboard
	b.bot.Send(msg)

	return nil
}

func (b *Botctl) registerDistrict(query *tgbotapi.CallbackQuery) error {
	username := query.From.UserName

	var userValue models.UserData
	resp, err := b.redis.Get(username).Result()
	if err != nil {
		return errRedisGet
	}

	err = json.Unmarshal([]byte(resp), &userValue)
	if err != nil {
		return errMarshal
	}

	userValue.District = query.Data

	userValueStr, err := json.Marshal(&userValue)
	if err != nil {
		log.Println("ERROR [registerCity] marshal user value: ", err)
		return errMarshal
	}

	err = b.redis.Set(username, userValueStr, 0).Err()
	if err != nil {
		log.Println("ERROR [registerCity] saving record to redis: ", err)
		return errRedisSet
	}

	var keyboard tgbotapi.InlineKeyboardMarkup

	switch query.Data {
	case "центральний":
		keyboard = tgbotapi.NewInlineKeyboardMarkup(
			tgbotapi.NewInlineKeyboardRow(
				tgbotapi.NewInlineKeyboardButtonData("Хенсонська", "хенсонська"),
			),
		)
	case "новокодацький":
		keyboard = tgbotapi.NewInlineKeyboardMarkup(
			tgbotapi.NewInlineKeyboardRow(
				tgbotapi.NewInlineKeyboardButtonData("Метробудівська", "метробудівська"),
			),
		)
	case "веселе":
		keyboard = tgbotapi.NewInlineKeyboardMarkup(
			tgbotapi.NewInlineKeyboardRow(
				tgbotapi.NewInlineKeyboardButtonData("Гагаріна", "гагаріна"),
			),
		)
	case "синельникове":
		keyboard = tgbotapi.NewInlineKeyboardMarkup(
			tgbotapi.NewInlineKeyboardRow(
				tgbotapi.NewInlineKeyboardButtonData("Ягідна", "ягідна"),
			),
		)
	}

	b.bot.AnswerCallbackQuery(tgbotapi.NewCallback(query.ID, "Збережено!"))

	msg := tgbotapi.NewMessage(query.Message.Chat.ID, "Оберіть свою вулицю")
	msg.ReplyMarkup = keyboard
	b.bot.Send(msg)

	return nil
}

func (b *Botctl) registerStreet(query *tgbotapi.CallbackQuery) error {
	username := query.From.UserName

	var userValue models.UserData
	resp, err := b.redis.Get(username).Result()
	if err != nil {
		return errRedisGet
	}

	err = json.Unmarshal([]byte(resp), &userValue)
	if err != nil {
		return errMarshal
	}

	userValue.Street = query.Data

	userValueStr, err := json.Marshal(&userValue)
	if err != nil {
		log.Println("ERROR [registerCity] marshal user value: ", err)
		return errMarshal
	}

	err = b.redis.Set(username, userValueStr, 0).Err()
	if err != nil {
		log.Println("ERROR [registerCity] saving record to redis: ", err)
		return errRedisSet
	}

	b.bot.AnswerCallbackQuery(tgbotapi.NewCallback(query.ID, "Збережено!"))

	msg := tgbotapi.NewMessage(
		query.Message.Chat.ID,
		fmt.Sprintf("Зроблено підписку на адресу: \n%s - %s - %s", userValue.City, userValue.District, userValue.Street),
	)
	b.bot.Send(msg)

	return nil
}
