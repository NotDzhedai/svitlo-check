package botctl

import (
	"github.com/go-redis/redis"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/jasonlvhit/gocron"
	"gitlab.com/NotDzhedai/svitlo-check/internal/config"
)

type Botctl struct {
	bot   *tgbotapi.BotAPI
	cfg   *config.Config
	redis *redis.Client
	gc    *gocron.Scheduler
}

func NewTelegramControl(cfg *config.Config, redis *redis.Client) (*Botctl, error) {
	// Creating bot
	bot, err := tgbotapi.NewBotAPI(cfg.TelegramToken)
	if err != nil {
		return nil, err
	}

	// Creating sceduler
	gc := gocron.NewScheduler()

	// Creating bot control
	return &Botctl{
		bot:   bot,
		redis: redis,
		cfg:   cfg,
		gc:    gc,
	}, nil
}

func (b *Botctl) RunBot() error {
	b.bot.Debug = b.cfg.TelegramDebug

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := b.bot.GetUpdatesChan(u)
	if err != nil {
		return err
	}

	b.gc.Every(1).Day().At(b.cfg.TimeToSendData).Do(b.sendInfoToUsers)
	// tc.gc.Every(1).Minutes().From(gocron.NextTick()).Do(tc.sendInfoToUsers)
	go b.gc.Start()

	for update := range updates {
		if update.CallbackQuery != nil {
			if err := b.handleCallbackQuery(update.CallbackQuery); err != nil {
				b.handleErrors(update.Message.Chat.ID, err)
			}
		}

		if update.Message == nil {
			continue
		}

		// just for developing. blocking all users but me
		if b.cfg.AppMode == "development" {
			if update.Message.From.UserName != "NotDzhedai" {
				b.bot.Send(tgbotapi.NewMessage(update.Message.Chat.ID, "Developing... Try again later"))
				continue
			}
		}

		// Handle commands
		if update.Message.IsCommand() {
			if err := b.handleCommands(update.Message); err != nil {
				// Handle errors
				b.handleErrors(update.Message.Chat.ID, err)
			}
			continue
		}

		// Handle messages
		if err := b.handleMessage(update.Message); err != nil {
			b.handleErrors(update.Message.Chat.ID, err)
		}
	}

	return nil
}
