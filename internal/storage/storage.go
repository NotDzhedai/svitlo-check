package storage

import (
	"github.com/patrickmn/go-cache"
)

var Storage *cache.Cache

func init() {
	Storage = cache.New(0, 0)
}

func GetStorage() *cache.Cache {
	return Storage
}
